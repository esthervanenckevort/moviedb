/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.moviedb;

import nl.allthingsdigital.moviedb.consumers.MostMoviesDuringLifetime;
import nl.allthingsdigital.moviedb.consumers.MostMoviesPerYear;
import nl.allthingsdigital.moviedb.consumers.YearWithMostMovies;
import nl.allthingsdigital.moviedb.consumers.DistinctMovieCounter;
import nl.allthingsdigital.moviedb.consumers.MoviesForNamedActor;
import nl.allthingsdigital.moviedb.consumers.YearsWithoutMovies;
import nl.allthingsdigital.moviedb.consumers.MovieCounter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public class MovieDB {

    private static final Logger LOG = Logger.getLogger(MovieDB.class.getName());

    public static void main(final String[] args) {
        MovieDB main = new MovieDB();
        for (final String file : args) {
            try {
                main.run(file);
            } catch (final IOException ex) {
                LOG.log(Level.WARNING, "I/O error {1} reading {0}", new Object[] { file, ex.getMessage() });
            }
        }
    }

    private void run(final String file) throws IOException {
        final Stream<String> movies = Files.lines(Paths.get(file));
        final MovieDBConsumer consumer = new MovieDBConsumer();
        consumer.addConsumer(new MovieCounter());
        consumer.addConsumer(new DistinctMovieCounter());
        consumer.addConsumer(new YearWithMostMovies());
        consumer.addConsumer(new YearsWithoutMovies());
        consumer.addConsumer(new MostMoviesDuringLifetime());
        consumer.addConsumer(new MostMoviesPerYear());
        consumer.addConsumer(new MoviesForNamedActor("Eastwood, Clint"));
        movies.forEach(consumer);
        consumer.report();
    }
}
