/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.moviedb;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import nl.allthingsdigital.moviedb.consumers.MovieConsumer;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class MovieDBConsumer implements Consumer<String> {
    private final List<MovieConsumer> consumers = new ArrayList<>();
    private static final String SEPARATOR = "/";

    @Override
    public void accept(String line) {
        final String[] parts = line.split(SEPARATOR, 2);
        final String[] actors = parts[1].split(SEPARATOR);
        final int yearStart = parts[0].lastIndexOf('(');
        final int year;
        year = Integer.parseInt(parts[0].substring(yearStart + 1, yearStart + 5));
        final String title = parts[0].substring(0, yearStart);
        consumers.forEach(c -> c.accept(title, year, actors));
    }

    public void report() {
        consumers.forEach(c -> c.report());
    }

    public void addConsumer(MovieConsumer consumer) {
        consumers.add(consumer);
    }

}
