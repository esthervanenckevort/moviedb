/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.moviedb.consumers;

import nl.allthingsdigital.moviedb.consumers.util.ActorYear;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class MostMoviesPerYear extends AbstractMovieConsumer {
    Map<ActorYear, Integer> moviesByActorPerYear = new HashMap<>();

    @Override
    public void accept(String title, int year, String[] actors) {
        for (final String actor : actors) {
            ActorYear key = new ActorYear(actor, year);
            moviesByActorPerYear.compute(key, (K, V ) -> V == null ? 1 : V + 1);
        }
    }

    @Override
    public void report() {
        Object[] triple = {"", 0, 0};
        moviesByActorPerYear.forEach((K, V ) -> {
            if (V > (Integer) triple[2]) {
                triple[0] = K.getActor();
                triple[1] = K.getYear();
                triple[2] = V;
            }
        });
        LOG.log(Level.INFO, "The actor with most movies in one year was {0} in {1} with {2} movies", triple);
    }
}
