/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.moviedb.consumers;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class YearWithMostMovies extends AbstractMovieConsumer {
    final Map<Integer, Integer> moviesPerYear = new HashMap<>();

    @Override
    public void accept(String title, int year, String[] actors) {
        moviesPerYear.compute(year, (K, V) -> V == null ? 1 : V + 1);
    }

    @Override
    public void report() {
        Integer[] tuple = {0, 0};
        moviesPerYear.forEach((K, V) -> {
            if (V > tuple[1]) {
                tuple[0] = K;
                tuple[1] = V;
            }
        });
        LOG.log(Level.INFO, "Year with most movies was {0} with {1} movies.", tuple);
    }
}
