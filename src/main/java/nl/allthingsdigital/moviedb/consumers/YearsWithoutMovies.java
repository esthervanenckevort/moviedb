/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.moviedb.consumers;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class YearsWithoutMovies extends AbstractMovieConsumer {
    private final List<Integer> years = new ArrayList<>();

    @Override
    public void accept(String title, int year, String[] actors) {
        years.add(year);
    }

    @Override
    public void report() {
        IntSummaryStatistics stats = years.stream().collect(Collectors.summarizingInt((Y) -> Y));
        List<Integer> range = IntStream.rangeClosed(stats.getMin(), stats.getMax()).boxed().collect(Collectors.toList());
        range.removeAll(years);
        LOG.log(Level.INFO, "Years without movies: {0}", range);
    }

}
