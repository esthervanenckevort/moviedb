/*
 * Copyright 2014 David van Enckevort <david@allthingsdigital.nl>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.allthingsdigital.moviedb.consumers.util;

import java.util.Objects;

/**
 * Helper class to hold the actor and year. This is used to compute statistics
 * per actor / year.
 *
 * @author David van Enckevort <david@allthingsdigital.nl>
 */
public final class ActorYear {

    /**
     * The actor.
     */
    private String actor;
    /**
     * The year.
     */
    private Integer year;

    /**
     * Constructor.
     *
     * @param actorValue the name of the actor.
     * @param yearValue the year the actor was active.
     */
    public ActorYear(final String actorValue, final Integer yearValue) {
        actor = actorValue;
        year = yearValue;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActorYear other = (ActorYear) obj;
        if (!Objects.equals(this.actor, other.actor)) {
            return false;
        }
        return Objects.equals(this.year, other.year);
    }

    /**
     * Getter.
     * @return the actor.
     */
    public String getActor() {
        return actor;
    }

    /**
     * Setter.
     * @param value the new value.
     */
    public void setActor(final String value) {
        actor = value;
    }

    /**
     * Getter.
     * @return the year.
     */
    public Integer getYear() {
        return year;
    }

    /**
     * Setter.
     * @param value the new value.
     */
    public void setYear(final Integer value) {
        year = value;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.actor);
        hash = 47 * hash + Objects.hashCode(this.year);
        return hash;
    }
}
